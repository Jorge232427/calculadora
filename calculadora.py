#Crear una calculadora con funciones basicas 

class Calculadora:
    def __init__(self):
        self.operando1=0
        self.operando2=0
        self.resultado=0
    def sumar(self):
        self.resultado=self.operando1+self.operando2
    def restar(self):
        self.resultado=self.operando1-self.operando2
    def multiplicar(self):
        self.resultado=self.operando1*self.operando2
    def dividir(self):
        self.resultado=self.operando1/self.operando2
    def modulo(self):
        self.resultado=self.operando1%self.operando2
